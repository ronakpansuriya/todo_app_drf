from django.contrib.auth.models import User, Group
from rest_framework.response import Response
from .models import *
from rest_framework import viewsets
from rest_framework.views import APIView 
from rest_framework import permissions
from app.serializers import *
# from .serializers import *



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class Employee(APIView):
    """
    Fetch Employe data API
    """
    def get(self, request):
        datas = Data.objects.all()
        serilizer = DataSerialixer(datas, many=True)
        return Response(serilizer.data)
    
    def post(self, request):
        pass